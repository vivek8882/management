# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2020 Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api, fields,tools, models,_
from odoo.exceptions import ValidationError

class CarDiagnosisWizard(models.TransientModel):
    _name = 'car.diagnosis.wizard'
    _description = "Car Diagnosis Wizard Detail"

    technician_id = fields.Many2one('res.users',"Mechanic")
    car_id = fields.Many2one('car.car',"Car License Plate")

    @api.onchange('car_id')
    def onchange_car_id(self):
        car_lines_ids = []
        car_service_id = self.env.context.get('active_id')
        for record in self:
            if record.car_id:
                car_service_id = self.env.context.get('active_id')
                car_service_id = self.env['car.service'].search([('id','=',car_service_id)])
                for car_id in car_service_id.car_ids:
                    if car_id:
                        car_detail_id = self.env['car.car'].search([('id','=',car_id.car_id.id)])
                        car_lines_ids.append(car_detail_id)
                if record.car_id not in car_lines_ids:
                    raise ValidationError('Please select your own car')                   

    #Create car diagnosis from car service wizard.
    def create_car_diagnosis(self):
        car_service_id = self.env.context.get('active_id')
        car_service_id = self.env['car.service'].search([('id','=',car_service_id)])
        if car_service_id.state == 'receive':
            car_service_id.state = 'diagnosis'
        for car_id in car_service_id.car_ids:
            if car_id:
                car_detail_ids = self.env['car.car'].search([('id','=',car_id.car_id.id)])
                for car_detail_id in car_detail_ids:
                    if car_detail_id.id == self.car_id.id:
                        car_line_obj = self.env['car.lines'].search([('id','=',car_id.id)])
                        write_mechanic_in_car_lines = car_line_obj.write({'mechanic_id':self.technician_id.id})
        assigned_mechanic_list = []
        for car_id in car_service_id.car_ids:
            if car_id.mechanic_id:
                assigned_mechanic_list.append(car_id.mechanic_id.id)
        if len(car_service_id.car_ids) == len(assigned_mechanic_list):
            car_service_id.asigned_all_car_mechanic = True
        else:
            car_service_id.asigned_all_car_mechanic = False
        car_service_dict = {'car_service_id':car_service_id.id,
                            'partner_id':car_service_id.partner_id.id,
                            'priority':car_service_id.priority,
                            'assigned_to':car_service_id.assigned_to.id,
                            'receipt_date':car_service_id.receipt_date,
                            'contact_name':car_service_id.contact_name,
                            'phone':car_service_id.phone,
                            'mobile':car_service_id.mobile,
                            'email':car_service_id.email,
                            'technician_id':self.technician_id.id,
                            'car_license_plate_id':self.car_id.id,
        }        
        diagnosis_obj = self.env['car.diagnosis']
        create_diagnosis = diagnosis_obj.create(car_service_dict)
        return create_diagnosis


