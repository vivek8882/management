# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2020-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api, fields, models,_
from odoo.exceptions import ValidationError

class CarDiagnosis(models.Model):
    _name = "car.diagnosis"
    _description = "Car Diagnosis Detail"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Name")
    partner_id = fields.Many2one('res.partner',"Customer")
    priority = fields.Selection([("1",'1'),("2",'2'),("3",'3'),("4",'4'),("5",'5')],"Priority")
    assigned_to = fields.Many2one('res.users',"Assigned Technician")
    receipt_date = fields.Date("Date of Receipt")
    contact_name = fields.Char("Contact Name")
    phone = fields.Char("Phone")
    mobile = fields.Char("Mobile")
    email = fields.Char("Email")
    diagnosis_details = fields.Text("Diagnosis Details")
    technician_id = fields.Many2one('res.users',"Mechanic")
    car_license_plate_id = fields.Many2one('car.car',"Car License Number")
    car_service_id = fields.Many2one('car.service', "Car service")
    spare_part_line_ids = fields.One2many('spare.part.lines','car_diagnosis_id', stirng="Spare Parts")
    active_work_order = fields.Boolean("Created Work order", help="If work order was created then this boolean field was true")
    state = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress','In Progress'),
        ('complete', 'Complete'),
        ], string='Status', default="draft")

    #Create sequance number.
    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].sudo().get('car.diagnosis') or ' '
        vals['name'] = sequence
        result = super(CarDiagnosis, self).create(vals)
        return result

        
    #Create work order from car diagnosis.
    def create_work_order(self):
        if self.active_work_order == False:
           self.active_work_order = True
        if self.state == 'draft':
            self.state = 'in_progress'
        for record in self:
            if record:
                spare_part_id_list = []
                for spare_part_id in record.spare_part_line_ids:
                    if spare_part_id:
                        spare_part_dict = {
                                           'product_id':spare_part_id.product_id.id,
                                           'product_code':spare_part_id.product_code,
                                           'product_quantity':spare_part_id.product_quantity,
                                           'product_price':spare_part_id.product_price,
                                           'product_type':spare_part_id.product_type,
                                           'subtotal':spare_part_id.subtotal,
                        }
                        spare_part_id_list.append((0,0, spare_part_dict))
                if spare_part_id_list:
                    work_order_dict = {'car_service_id':record.car_service_id.id,
                                       'car_diagnosis_id':record.id,
                                       'technician_id':record.technician_id.id,
                                       'partner_id':record.partner_id.id,
                                       'priority':record.priority,
                                       'receipt_date':record.receipt_date,
                                       'contact_name':record.contact_name,
                                       'car_license_plate_id':record.car_license_plate_id.id,
                                       'diagnosis_details':record.diagnosis_details,
                                       'spare_part_line_ids':spare_part_id_list,
                    }
                    work_orders_obj = self.env['work.orders']
                    create_work_order = work_orders_obj.create(work_order_dict)
                    return create_work_order
                else:
                    raise ValidationError('Please add spare parts for car service')


class SparePartlines(models.Model):
    _name = "spare.part.lines"
    _description = "SparePart Details"

    product_id = fields.Many2one('product.product', "Spare Part")
    product_code = fields.Char("Spare Part Code")
    product_quantity = fields.Float("Quantity")
    product_price = fields.Float("Unit Price")
    product_type = fields.Char("Spare Part Type")
    subtotal = fields.Float("Subtotal", compute="compute_subtotal")
    product_categ_id = fields.Many2one('product.category',"Product Category")
    car_diagnosis_id = fields.Many2one('car.diagnosis', "Diagnosis")
    
    def compute_subtotal(self):
        for record in self:
            if record.product_id and record.product_quantity and record.product_price:
                record.subtotal = record.product_quantity * record.product_price

    @api.onchange('product_id')
    def onchange_product_id(self):
        for record in self:
            if record.product_id:
                record.product_code = record.product_id.default_code
                record.product_price = record.product_id.list_price
                record.product_type = record.product_id.type
                record.product_categ_id = record.product_id.categ_id


