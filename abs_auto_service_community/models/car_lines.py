# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2020-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api, fields, models,_
from datetime import date

class CarLines(models.Model):
    _name = "car.lines"
    _description = "Car line Detail"

    car_id = fields.Many2one('car.car',"Car License Plate")
    car_brand = fields.Char("Car Brand")
    car_model = fields.Char("Car Model")
    chassis_number = fields.Char("Chassis Number")
    fuel_type = fields.Selection([('petrol',"Petrol"),('diesel',"Diesel")],"Fuel Type")
    under_guarantee = fields.Selection([('yes',"Yes"),('no',"No")],"Under Guarantee")
    guarantee_type = fields.Selection([('paid',"Paid"),('free',"Free")],"Guarantee Type")
    nature_of_service = fields.Selection([('full_service',"Full Service"),('half_service',"Half Service")],"Nature Of Service")
    note = fields.Text("Service Details")
    mechanic_id = fields.Many2one('res.users',"Mechanic")
    car_service_id = fields.Many2one('car.service',"Car Service")

    @api.onchange('car_id')
    def onchange_car_id(self):
        for record in self:
            if record.car_id:
                car_line_obj = self.env['car.car'].search([('id','=',record.car_id.id)])
                if car_line_obj:
                    record.car_brand = car_line_obj.car_brand
                    record.car_model = car_line_obj.car_model
                    record.chassis_number = car_line_obj.chassis_number
                    record.fuel_type = car_line_obj.fuel_type
                    record.under_guarantee = car_line_obj.under_guarantee
                    record.guarantee_type = car_line_obj.guarantee_type
                    record.nature_of_service = car_line_obj.nature_of_service
                    record.note = car_line_obj.note

