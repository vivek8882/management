# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2020-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api, fields, models,_
from datetime import date

class CarService(models.Model):
    _name = "car.service"
    _description = "Car Service Detail"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Service Number")
    partner_id = fields.Many2one('res.partner',"Customer")
    priority = fields.Selection([("1",'1'),("2",'2'),("3",'3'),("4",'4'),("5",'5')],"Priority")
    assigned_to = fields.Many2one('res.users',"Assigned Technician")
    receipt_date = fields.Date("Date of Receipt")
    contact_name = fields.Char("Contact Name")
    phone = fields.Char("Phone")
    mobile = fields.Char("Mobile")
    email = fields.Char("Email")
    car_ids = fields.One2many('car.lines','car_service_id', string="Cars")
    asigned_all_car_mechanic = fields.Boolean('Hide assign mechanic button')
    state = fields.Selection([
        ('receive', 'Received'),
        ('diagnosis', 'In Diagnosis'),
        ('work_in_progress','Work in Progress'),
        ('done','Done'),
        ], string='Status', default="receive")

    #Create sequance number.
    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].sudo().get('car.service') or ' '
        vals['name'] = sequence
        result = super(CarService, self).create(vals)
        return result

    @api.onchange('partner_id')
    def onchange_partner_id(self):
        for record in self:
            if record.partner_id:
                res_partner_obj = self.env['res.partner'].search([('id','=',record.partner_id.id)])
                if res_partner_obj:
                    record.contact_name = res_partner_obj.name
                    record.phone = res_partner_obj.phone
                    record.mobile = res_partner_obj.mobile
                    record.email = res_partner_obj.email

