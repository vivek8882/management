# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2020-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
from odoo import api, fields, models,_

class AccountInvoice(models.Model):
    _inherit = "account.move"

    is_work_order = fields.Boolean("From work order", default=False)

    #If work orders all invoices are paid then it will change the state of work order into 'paid' state. 
    def write(self, vals):
        result = super(AccountInvoice, self).write(vals)
        for record in self:
            if record.state:
                sale_order_obj = self.env['sale.order'].search([('name','=', record.invoice_origin)])
                paid_invoice_list = []
                for invoice_id in sale_order_obj.invoice_ids:
                    if invoice_id.state == 'posted':
                        paid_invoice_list.append(invoice_id.id) 
                if len(paid_invoice_list) == len(sale_order_obj.invoice_ids):
                    work_order_obj = self.env['work.orders'].search([('sale_order_id','=',sale_order_obj.name)])
                    work_order_obj.write({'state':'paid'})
                else:
                    work_order_obj = self.env['work.orders'].search([('sale_order_id','=',sale_order_obj.name)])
                    work_order_obj.write({'state':'finish'})
        return result
