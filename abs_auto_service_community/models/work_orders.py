# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2020-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api, fields, models,_
import time
from datetime import datetime

class CarWorkOrders(models.Model):
    _name = "work.orders"
    _description = "Work Order Detail"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char("Name")
    partner_id = fields.Many2one('res.partner',"Customer")
    priority = fields.Selection([("1",'1'),("2",'2'),("3",'3'),("4",'4'),("5",'5')],"Priority")
    receipt_date = fields.Date("Date of Receipt")
    contact_name = fields.Char("Contact Name")
    diagnosis_details = fields.Text("Diagnosis Details")
    technician_id = fields.Many2one('res.users',"Mechanic")
    car_license_plate_id = fields.Many2one('car.car',"Car License Number")
    car_service_id = fields.Many2one('car.service', "Car Service")
    car_diagnosis_id = fields.Many2one('car.diagnosis', "Car Diagnosis")
    planned_scheduled_date = fields.Datetime("Scheduled Date")
    planned_end_date = fields.Datetime("End Date")
    number_of_days = fields.Char("Number of Days", compute="compute_number_of_hours")
    start_date = fields.Datetime("Start Date")
    end_date = fields.Datetime("End Date")
    duration = fields.Char("Duration", compute="compute_duration")
    worked_hours = fields.Float("Worked Hours")
    spare_part_line_ids = fields.Many2many('spare.part.lines', stirng="Spare Parts")
    sale_order_id = fields.Many2one('sale.order',"Sale Order")
    active_quotation_order = fields.Boolean("Quotation", help="If quotation was created then this boolean field was true")
    state = fields.Selection([
        ('cancel','Cancelled'),
        ('set_to_draft','Set to Draft'),
        ('draft', 'Draft'),
        ('pending','Pending'),
        ('in_progress','In Progress'),
        ('finish', 'Finished'),
        ('paid','Payment Done'),
        ], string='Status', default="draft")

    #Create Quotation from work order.
    def create_quotation(self):
        if self.active_quotation_order == False:
           self.active_quotation_order = True
        for record in self:
            if record:
                quotation_create_date = datetime.now()      
                spare_part_list = []
                for spare_part_id in record.spare_part_line_ids:
                    if spare_part_id:
                        spare_part_dict = {'product_id':spare_part_id.product_id.id,
                                           'name':spare_part_id.product_id.name,
                                           'product_uom_qty':spare_part_id.product_quantity,
                                           'price_unit':spare_part_id.product_price,
                        }
                        spare_part_list.append((0,0, spare_part_dict))
                if record.worked_hours >= 1:
                    service_product_id = self.env.ref('abs_auto_service_community.auto_service_order_id_11')
                    service_product_dict = {'product_id':service_product_id.id,
                                            'name':service_product_id.name,
                                            'product_uom_qty':self.worked_hours,
                                            'price_unit':service_product_id.list_price,
                    }
                    spare_part_list.append((0,0, service_product_dict))
                sale_order_obj = self.env['sale.order']
                quotation_dict = {'partner_id':record.partner_id.id,
                                  'date_order':quotation_create_date,
                                  'order_line':spare_part_list,
                                  'origin':self.name,
                                  'is_work_order':True,
                }
                create_quotation = sale_order_obj.create(quotation_dict)
                if create_quotation:
                    self.sale_order_id = create_quotation.id 
                return create_quotation

    #It display number of days.
    @api.depends('planned_scheduled_date','planned_end_date')
    def compute_number_of_hours(self):
        for record in self:
            if record.number_of_days:
                continue
            else:
                if record.planned_scheduled_date and record.planned_end_date:
                    planned_scheduled_date_obj = datetime.strptime(str(record.planned_scheduled_date), '%Y-%m-%d %H:%M:%S')
                    planned_end_date_obj = datetime.strptime(str(record.planned_end_date), '%Y-%m-%d %H:%M:%S')
                    duration = planned_end_date_obj - planned_scheduled_date_obj
                    record.number_of_days = duration


    @api.depends('start_date','end_date')
    def compute_duration(self):
        for record in self:
            if record.duration:
                continue
            else:
                if record.end_date:
                    if record.start_date < record.end_date:
                        start_date_obj = datetime.strptime(str(record.start_date).split(".")[0], '%Y-%m-%d %H:%M:%S')
                        end_date_obj = datetime.strptime(str(record.end_date).split(".")[0], '%Y-%m-%d %H:%M:%S')
                        duration = end_date_obj - start_date_obj
                        record.duration = duration
                else:
                    pass

    def start_work_order(self):
        if self.state == 'draft':
            self.state = 'in_progress'
            self.start_date = datetime.now()
        for record in self:
            if record.car_license_plate_id:
                car_service_obj = self.env['car.service'].search([('id','=',record.car_service_id.id)])
                car_service_obj.write({'state':'work_in_progress'})

    def cancel_work_order(self):
        if self.state == 'draft':
            self.state = 'cancel'

    def set_as_draft(self):
        if self.state == 'cancel':
            self.state = 'draft'
            
    def pending_work_order(self):
        if self.state == 'in_progress':
            self.state = 'pending'

    def resume_work_order(self):
        if self.state == 'pending':
            self.state = 'in_progress'

    def finished_work_order(self):
        if self.state == 'in_progress':
            self.state = 'finish'
            self.end_date = datetime.now()   
        for record in self:
            if record.state == 'finish':
                car_diagnosis_obj = self.env['car.diagnosis'].search([('id','=',record.car_diagnosis_id.id)])
                car_diagnosis_obj.write({'state':'complete'})   

    #If work order is in 'paid' state then the car service state goes to "done".
    def write(self, vals):
        result = super(CarWorkOrders, self).write(vals)        
        for record in self:
            if record.state == 'paid':
                work_order_obj = self.env['work.orders'].search([('car_service_id','=',record.car_service_id.id),('state','=','paid')]) 
                if work_order_obj:
                    car_service_obj = self.env['car.service'].search([('id','=',record.car_service_id.id)])
                    if len(work_order_obj) == len(car_service_obj.car_ids):
                        car_service_obj.write({'state':'done'})     
        return result

    #Create sequence number.
    @api.model
    def create(self, vals):
        sequence = self.env['ir.sequence'].sudo().get('work.orders') or ' '
        vals['name'] = sequence
        result = super(CarWorkOrders, self).create(vals)
        return result

