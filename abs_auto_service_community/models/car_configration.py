# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2020-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api, fields, models,_

#Here config the car details.
class CarCar(models.Model):
    _name = "car.car"
    _description = "Car Details"

    name = fields.Char("Car License Plate")
    car_brand = fields.Char("Car Brand")
    car_model = fields.Char("Car Model")
    chassis_number = fields.Char("Chassis Number")
    fuel_type = fields.Selection([('petrol',"Petrol"),('diesel',"Diesel")],"Fuel Type")
    under_guarantee = fields.Selection([('yes',"Yes"),('no',"No")],"Under Guarantee")
    guarantee_type = fields.Selection([('paid',"Paid"),('free',"Free")],"Guarantee Type")
    nature_of_service = fields.Selection([('full_service',"Full Service"),('half_service',"Half Service")],"Nature Of Service")
    note = fields.Text("Service Details")
