# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2020-Today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
{
    'name'        : "Auto Service (Odoo v13 Community)",
    'author'      : 'Ascetic Business Solution',
    'category'    : 'Employee',
    'summary'     : """Vehicle Service Management System""",
    'website'     : 'http://www.asceticbs.com',
    'description' : """ """,
    'version'     : '1.0',
    'depends'     : ['base','sale','account'],
    'data'        : ['security/auto_spa_security.xml',
                     'security/ir.model.access.csv',
                     'wizard/car_diagnosis_wizard_view.xml',
                     'views/car_service_view.xml',
                     'views/car_configration_view.xml',
                     'views/car_lines_view.xml',
                     'views/car_diagnosis_view.xml',
                     'views/work_orders_view.xml',
                     'views/car_service_sale_order_view.xml',
                     'views/car_service_invoice_view.xml',
                     'views/report_car_service_invoice_view.xml',
                     'report/report_car_service_view.xml',
                     'report/report_car_service_template.xml',
                     'report/report_car_diagnosis_view.xml',
                     'report/report_car_diagnosis_template.xml',
                     'report/report_car_work_orders_view.xml',
                     'report/report_car_work_orders_template.xml',
                    ],
    'demo'        : ['data/product_demo.xml'],
    'live_test_url' : "http://www.test.asceticbs.com/web/database/selector",
    'images': ['static/description/banner.png'],
    'license': 'OPL-1',
    'price': 99.00,
    'currency': "EUR",
    'installable' : True,
    'application' : True,
    'auto_install': False,
}

